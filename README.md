# RPLidar Golang

Port of the [RPLidar Python module](https://github.com/SkoltechRobotics/rplidar) to Go.

## Example

```golang
package main

import (
	"fmt"

	l "gitlab.com/sijisu/rplidar"
)

func main() {
	// we want to use the defaults
	lidar, _ := l.NewRPLidar("", 0, false)

	status, errCode := lidar.GetHealth()
	model, firmware, hardware, sn := lidar.GetInfo()

	fmt.Printf("HEALTH: status: %s, code: %d\n", status, errCode)
	fmt.Printf("INFO: model: %d, firmware: %d.%d, hardware: %d, S/N: %s\n", model, firmware[0], firmware[1], hardware, sn)

	mes, stop := lidar.IterMeasurments(0)

	for i := range mes {
		fmt.Printf("mes: %t %d %f %f\n", i.IsNewScan, i.Quality, i.Angle, i.Distance)
	}
	// stop the measuring
	close(stop)
}
```

## Todo

 - implement more things (ExpressScan etc.) according to the [spec](http://bucket.download.slamtec.com/ccb3c2fc1e66bb00bd4370e208b670217c8b55fa/LR001_SLAMTEC_rplidar_protocol_v2.1_en.pdf)
 - error handeling