package rpilidar

import (
	"errors"
	"log"
	"time"

	"go.bug.st/serial"
)

func readTimeout(p serial.Port, size int, timeout time.Duration) ([]byte, int, error) {
	var output []byte
	startTime := time.Now()
	var readCount int
	for {
		tmp := make([]byte, 1)
		n, err := p.Read(tmp)
		if err != nil {
			log.Fatal(err)
		}
		readCount += n
		//fmt.Printf("read %x, %d\n", tmp, readCount)

		if n > 0 {
			output = append(output, tmp[0])
		}

		if size == readCount {
			//fmt.Printf("returning %x, %d\n", output, readCount)
			return output, readCount, nil
		}

		if time.Since(startTime) > timeout {
			return nil, 0, errors.New("Serial read timeout")
		}

	}
}
