package rpilidar

import (
	"bytes"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"log"
	"time"

	"go.bug.st/serial"
)

const syncByte byte = '\xA5'
const syncByte2 byte = '\x5A'

const getInfoByte byte = '\x50'
const getHealthByte byte = '\x52'
const getSamplerateByte byte = '\x59'
const getLidarConfByte byte = '\x84'

const stopByte byte = '\x25'
const resetByte byte = '\x40'

const scanByte byte = '\x20'
const forceScanByte byte = '\x21'
const expressScanByte byte = '\x82'

const descriptorLen int = 7
const infoLen int = 20
const healthLen int = 3

const infoType int = 4
const healthType int = 6
const scanType int = 129

//Constants & Command to start A2 motor
const maxMotorPWM int = 1023
const defaultMotorPWM int = 660
const setPWMByte byte = '\xF0'

var healthStatuses = map[int]string{
	0: "Good",
	1: "Warning",
	2: "Error",
}

// RPLidar is the main struct for the library
type RPLidar struct {
	p              serial.Port
	port           string
	baudrate       int
	isRunning      bool
	isMotorRunning bool
}

// Measurement is the data sent by the lidar
type Measurement struct {
	IsNewScan bool
	Quality   int
	Angle     float64
	Distance  float64
}

func processScan(raw []byte) Measurement {
	newscan := bool((raw[0] & 0b1) == 1)
	inversednewscan := ((raw[0] >> 1) & 0b1) == 1
	qual := int(raw[0] >> 2)
	if newscan == inversednewscan {
		panic("New scan flags mismatch")
	}
	checkbit := raw[1] & 0b1
	if checkbit != 1 {
		panic("Check bit not equal to 1")
	}
	angl := float64(float64((uint16(raw[1])>>1)+(uint16(raw[2])<<7)) / 64.)
	dist := float64(float64(uint16(raw[3])+(uint16(raw[4])<<8)) / 4.)
	return Measurement{newscan, qual, angl, dist}
}

// NewRPLidar generates new RPLidar object based on the parameters and connects to it
func NewRPLidar(port string, baudrate int, debuglogging bool) (*RPLidar, error) {
	if !debuglogging {
		log.SetOutput(ioutil.Discard)
	}
	if port == "" {
		port = "/dev/ttyUSB0"
	}

	if baudrate == 0 {
		baudrate = 115200
	}

	isMotorRunning := false

	r := RPLidar{port: port, baudrate: baudrate, isMotorRunning: isMotorRunning}

	r.connect()
	r.StartMotor()

	return &r, nil
}

func (r *RPLidar) connect() {
	if r.p != nil {
		r.disconnect()
	}

	mode := &serial.Mode{
		BaudRate: r.baudrate,
		Parity:   serial.NoParity,
		DataBits: 8,
		StopBits: serial.OneStopBit,
	}

	p, err := serial.Open(r.port, mode)
	if err != nil {
		panic(err)
	}
	p.SetDTR(false)

	r.p = p
}

func (r *RPLidar) disconnect() {
	if r.p == nil {
		return
	}
	r.p.Close()
	r.p = nil
}

// SetPWN is used to set PWM of the lidar motor
func (r *RPLidar) SetPWN(pwm int) {
	if 0 > pwm || pwm > maxMotorPWM {
		panic("PWM out of range")
	}
	payload := make([]byte, 2)
	binary.LittleEndian.PutUint16(payload, uint16(pwm))
	r.sendPayloadCmd(setPWMByte, payload)
}

// StartMotor on the lidar
func (r *RPLidar) StartMotor() {
	log.Println("Starting motor")

	// For A1
	r.p.SetDTR(false)

	// For A2
	r.SetPWN(defaultMotorPWM)
	r.isMotorRunning = true
}

// StopMotor on the lidar
func (r *RPLidar) StopMotor() {
	log.Println("Stoping motor")

	// For A2
	r.SetPWN(0)
	time.Sleep(1 * time.Millisecond)

	// For A1
	r.p.SetDTR(true)
	r.isMotorRunning = false
}

// Is the lidar running? Returns if the sensor is running and motor is running
func (r *RPLidar) IsRunning() (bool, bool) {
	return r.isRunning, r.isMotorRunning
}

func (r *RPLidar) sendPayloadCmd(cmd byte, payload []byte) {
	lengthByte := byte(uint8(len(payload)))

	req := []byte{syncByte, cmd, lengthByte}

	req = append(req[:], payload[:]...)

	var checksum byte = 0

	for _, reqByte := range req {
		checksum ^= reqByte
	}

	req = append(req[:], checksum)

	n, err := r.p.Write(req)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Command sent: %x, %d\n", req, n)
}

func (r *RPLidar) sendCmd(cmd byte) {
	req := []byte{syncByte, cmd}

	n, err := r.p.Write(req)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Command sent: %x, %d\n", req, n)
}

func (r *RPLidar) readDescriptor() (int, bool, int) {

	buf := make([]byte, descriptorLen)

	n, err := r.p.Read(buf)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Received descriptor: %x, %d\n", buf, n)

	if len(buf) != descriptorLen {
		panic("Descriptor length mismatch")
	}

	if !bytes.HasPrefix(buf, []byte{syncByte, syncByte2}) {
		panic("Incorrect descriptor starting bytes")
	}

	var isSingle bool = (buf[len(buf)-2] == 0)

	return int(buf[2]), isSingle, int(buf[len(buf)-1])
}

func (r *RPLidar) readResponse(dsize int) []byte {
	log.Printf("Trying to read response: %d bytes\n", dsize)

	/* Implementing a custom readtimeout here, it is not ideal, but it works
	buf := make([]byte, dsize)
	n, err := r.p.Read(buf)*/
	buf, n, err := readTimeout(r.p, dsize, time.Second)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Received data: %x, %d\n", buf, n)

	if len(buf) != dsize {
		panic("Wrong body size")
	}

	return buf
}

// GetInfo about the lidar
func (r *RPLidar) GetInfo() (model int, firmware []int, hardware int, serialnumber string) {
	r.sendCmd(getInfoByte)
	dsize, isSingle, dtype := r.readDescriptor()
	if dsize != infoLen {
		panic("Wrong get_info reply length")
	}
	if !isSingle {
		panic("Not a single response mode")
	}
	if dtype != infoType {
		panic("Wrong response data type")
	}
	raw := r.readResponse(dsize)
	serialNumber := hex.EncodeToString(raw[4:])
	return int(raw[0]), []int{int(raw[2]), int(raw[1])}, int(raw[3]), serialNumber
}

// GetHealth status of the lidar
func (r *RPLidar) GetHealth() (string, int) {
	r.sendCmd(getHealthByte)
	dsize, isSingle, dtype := r.readDescriptor()
	if dsize != healthLen {
		panic("Wrong get_health reply length")
	}
	if !isSingle {
		panic("Not a single response mode")
	}
	if dtype != healthType {
		panic("Wrong response data type")
	}
	raw := r.readResponse(dsize)
	status := healthStatuses[int(raw[0])]
	errorCode := (int(raw[1]) << 8) + int(raw[2])
	return status, errorCode
}

// Stop the scanning, turn off the diode
func (r *RPLidar) Stop() {
	r.isRunning = false
	log.Printf("Stoping scanning\n")
	r.sendCmd(stopByte)
	r.p.ResetInputBuffer()
	time.Sleep(time.Millisecond)
}

// Reset the lidar to its default state
func (r *RPLidar) Reset() {
	log.Printf("Reseting the sensor\n")
	r.isRunning = false
	r.isMotorRunning = true
	r.sendCmd(resetByte)
	time.Sleep(2 * time.Millisecond)
}

// IterMeasurments is used to get access to the individual measurements, uses a go routine to be asynchronous, pipes the measurements to channel
func (r *RPLidar) IterMeasurments(maxBufMeas uint) (<-chan Measurement, chan<- bool) {
	r.isRunning = true
	if maxBufMeas == 0 {
		maxBufMeas = 500
	}
	r.StartMotor()
	status, errorcode := r.GetHealth()
	log.Printf("Health status: %s [%d]", status, errorcode)
	if status == healthStatuses[2] {
		log.Fatalf("Trying to reset sensor due to the error. Error code: %d", errorcode)
		r.Reset()
		status, errorcode = r.GetHealth()
		if status == healthStatuses[2] {
			panic(fmt.Sprintf("RPLidar hardware failure.  Error code: %d", errorcode))
		}
	} else if status == healthStatuses[1] {
		log.Fatalf("Warning sensor status detected! Error code: %d", errorcode)
	}
	r.sendCmd(scanByte)
	dsize, isSingle, dtype := r.readDescriptor()
	if dsize != 5 {
		panic("Wrong IterMeasurments reply length")
	}
	if isSingle {
		panic("Not a multiple response mode")
	}
	if dtype != scanType {
		panic("Wrong response data type")
	}

	mes := make(chan Measurement, maxBufMeas)
	stop := make(chan bool)

	go func() {

		defer func() {
			close(mes)
			log.Println("Stopping")
			r.Stop()
		}()

		for {
			select {
			default:
				raw := r.readResponse(dsize)
				log.Printf("Received scan response: %x\n", raw)
				// if the channel is full
				if len(mes) == cap(mes) {
					log.Fatalf("Too many measurments in the input buffer channel: %d/%d. Poping last element", len(mes), cap(mes))
					// pop last measurement
					<-mes
				}
				mes <- processScan(raw)
			case <-stop:
				return
			}
		}
	}()
	return mes, stop
}

// IterScans is used to get access to the whole scan, uses a go routine to be asynchronous, pipes the scans (type []Measurement) to channel
func (r *RPLidar) IterScans(maxBufMeas uint, minLen int) (<-chan []Measurement, chan<- bool) {
	if maxBufMeas == 0 {
		maxBufMeas = 500
	}

	if minLen == 0 {
		minLen = 5
	}

	scn := make(chan []Measurement, maxBufMeas)
	stop := make(chan bool)

	mes, mesStop := r.IterMeasurments(maxBufMeas)

	var output []Measurement

	go func() {

		defer func() {
			close(scn)
			close(mesStop)
		}()

		for i := range mes {
			select {
			default:
				if i.IsNewScan {
					if len(output) > minLen {
						scn <- output
					}
					// empty output, keep allocated memory
					output = output[:0]
				}
				if i.Quality > 0 && i.Distance > 0 {
					output = append(output, i)
				}
			case <-stop:
				return
			}
		}
	}()
	return scn, stop
}
